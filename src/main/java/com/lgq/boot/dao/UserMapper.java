package com.lgq.boot.dao;

import com.lgq.boot.entity.User;
import com.lgq.boot.base.BaseDao;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;

/**
 *code is far away from bug with the animal protecting
 *  ┏┓　　　┏┓
 *┏┛┻━━━┛┻┓
 *┃　　　　　　　┃ 　
 *┃　　　━　　　┃
 *┃　┳┛　┗┳　┃
 *┃　　　　　　　┃
 *┃　　　┻　　　┃
 *┃　　　　　　　┃
 *┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　　┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 *　　
 *   @description : User Mapper 接口
 *   ---------------------------------
 * 	 @author Liang.Guangqing123
 *   @since 2018-01-16
 */
@Repository
public interface UserMapper extends BaseDao<User> {

    List<User> selectPageWithParam(Page<User> page,User record);

    User selectOneByObj(Long id);

}