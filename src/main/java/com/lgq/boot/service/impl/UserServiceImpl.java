package com.lgq.boot.service.impl;

import com.lgq.boot.entity.User;
import com.lgq.boot.dao.UserMapper;
import com.lgq.boot.service.UserService;
import com.lgq.boot.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 *code is far away from bug with the animal protecting
 *  ┏┓　　　┏┓
 *┏┛┻━━━┛┻┓
 *┃　　　　　　　┃ 　
 *┃　　　━　　　┃
 *┃　┳┛　┗┳　┃
 *┃　　　　　　　┃
 *┃　　　┻　　　┃
 *┃　　　　　　　┃
 *┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　　┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 *　　
 *   @description : User 服务实现类
 *   ---------------------------------
 * 	 @author Liang.Guangqing123
 *   @since 2018-01-16
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements UserService {
	
}
