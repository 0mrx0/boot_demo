/*
 Navicat Premium Data Transfer

 Source Server         : 39.108.58.69
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : 39.108.58.69:3306
 Source Schema         : tdx_elec

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 23/01/2018 15:32:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for elec_user
-- ----------------------------
DROP TABLE IF EXISTS `elec_user`;
CREATE TABLE `elec_user`  (
  `id` bigint(8) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `staff_id` bigint(8) NULL DEFAULT NULL COMMENT '员工ID',
  `type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户类型',
  `account` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登陆账号',
  `nickname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登陆密码',
  `last_login` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_dt` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_dt` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_account`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8889 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of elec_user
-- ----------------------------
INSERT INTO `elec_user` VALUES (1, NULL, '3', 'guangqing', '梁广庆', '$2a$10$fT2qPslAlPk4Hd6zoobe5uVOEGIhd2b2wG6mvFGu/q5RC1d.swvtS', NULL, 'admin', '2017-11-17 19:03:23', NULL, NULL);
INSERT INTO `elec_user` VALUES (2, NULL, '3', 'tdxgps', '监控中心', '$2a$10$Hma3Cpn5NXmYWUSBg5uMvuKVPu77yu0WGxklN5p0ySHgdy99lIbo2', NULL, 'admin', '2017-12-01 19:19:25', NULL, NULL);
INSERT INTO `elec_user` VALUES (3, NULL, '1', 'zuorong', '徐佐荣', '$2a$10$A2qbB3IBWSLXfVKRKaS53uSoEPaYVBWZ67bNUPnnrWmMW8tr7usCa', NULL, 'admin', '2017-12-21 10:12:37', NULL, NULL);
INSERT INTO `elec_user` VALUES (6666, NULL, '6666', 'tdx', '系统管理员', '$2a$10$9JrybG3Q1Ih7Fthrk0XIvOvm0yRFNGym/Sh9uU2XmQAhud0DwfvHi', NULL, 'admin', '2017-11-03 14:36:45', NULL, NULL);
INSERT INTO `elec_user` VALUES (8888, NULL, '8888', 'admin', '超级管理员', '$2a$10$M88r0Oos1J4q1l9wA7YQA.oj9AENLIA7oamZJf2jEqX97FEv1U4du', NULL, 'admin', '2017-11-03 14:13:27', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
